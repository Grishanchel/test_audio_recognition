# Предварительно нужно установить пакеты и импортировать токен для использования API
#
# pip3 install grpcio-tools
# $ python3 main.py --token "<access_token>" --file "<file>"
#
#https://developers.sber.ru/docs/ru/salutespeech/authentication?utm_source=support - здесь инструкция по получению токена

import argparse
import itertools
import time

import grpc

import recognition_pb2
import recognition_pb2_grpc

# Константы для обработки аудиоданных
CHUNK_SIZE = 2048
SLEEP_TIME = 0.1

# Кодировки аудио и их отображение на константы из protobuf
ENCODING_PCM = 'pcm'
ENCODINGS_MAP = {
    ENCODING_PCM: recognition_pb2.RecognitionOptions.PCM_S16LE,
    'opus': recognition_pb2.RecognitionOptions.OPUS,
    'mp3': recognition_pb2.RecognitionOptions.MP3,
    'flac': recognition_pb2.RecognitionOptions.FLAC,
    'alaw': recognition_pb2.RecognitionOptions.ALAW,
    'mulaw': recognition_pb2.RecognitionOptions.MULAW,
}

# Функция для печати 'x-request-id' из метаданных
def try_printing_request_id(md):
    for m in md:
        if m.key == 'x-request-id':
            print('RequestID:', m.value)

# Генерация аудиопорций из файла
def generate_audio_chunks(path, chunk_size=CHUNK_SIZE, sleep_time=SLEEP_TIME):
    with open(path, 'rb') as f: # здесь нужно вместо <path> вставить путь к файлу
        for data in iter(lambda: f.read(chunk_size), b''):
            yield recognition_pb2.RecognitionRequest(audio_chunk=data)
            time.sleep(sleep_time)

# Основная функция распознавания речи
def recognize(args):
    # Создание учетных данных SSL/TLS и токена доступа для gRPC
    ssl_cred = grpc.ssl_channel_credentials()
    token_cred = grpc.access_token_call_credentials(args.token)

    # Установка безопасного канала связи gRPC
    channel = grpc.secure_channel(
        args.host,
        grpc.composite_channel_credentials(ssl_cred, token_cred),
    )

    # Создание заглушки (stub) для вызовов методов службы SmartSpeech
    stub = recognition_pb2_grpc.SmartSpeechStub(channel)

    # Отправка запроса на распознавание с параметрами и аудиопорциями
    con = stub.Recognize(itertools.chain(
        (recognition_pb2.RecognitionRequest(options=args.recognition_options),),
        generate_audio_chunks(args.file),
    ))

    try:
        # Обработка ответов от сервера
        for resp in con:
            if not resp.eou:
                print('Получен частичный результат:')
            else:
                print('Получен результат конца высказывания:')

            for i, hyp in enumerate(resp.results):
                print('  Вариант #{}: {}'.format(i + 1, hyp.normalized_text if args.normalized_result else hyp.text))

            # Вывод эмоций при наличии результатов и конца высказывания
            if resp.eou and args.emotions_result:
                print('  Эмоции: pos={}, neu={}, neg={}'.format(
                    resp.emotions_result.positive,
                    resp.emotions_result.neutral,
                    resp.emotions_result.negative,
                ))
    except grpc.RpcError as err:
        # Обработка ошибок gRPC
        print('Ошибка RPC: код = {}, детали = {}'.format(err.code(), err.details()))
    except Exception as exc:
        # Обработка других исключений
        print('Исключение:', exc)
    else:
        # Вывод сообщения об окончании распознавания
        print('Распознавание завершено')
    finally:
        # Печать 'x-request-id' и закрытие канала
        try_printing_request_id(con.initial_metadata())
        channel.close()

# Класс для обработки аргументов командной строки
class Arguments:
    NOT_RECOGNITION_OPTIONS = {'host', 'token', 'file', 'normalized_result', 'emotions_result'}
    NOT_RECOGNITION_OPTIONS.update({'ca', 'cert', 'key'})  # дополнительные параметры
    DURATIONS = {'no_speech_timeout', 'max_speech_timeout', 'eou_timeout'}
    REPEATED = {'words'}
    HINTS_PREFIX = 'hints_'

    def __init__(self):
        super().__setattr__('recognition_options', recognition_pb2.RecognitionOptions())

    def __setattr__(self, key, value):
        if key in self.NOT_RECOGNITION_OPTIONS:
            super().__setattr__(key, value)
        elif key.startswith(self.HINTS_PREFIX):
            key = key[len(self.HINTS_PREFIX):]
            self._set_option(self.recognition_options.hints, key, value)
        else:
            self._set_option(self.recognition_options, key, value)

    def _set_option(self, obj, key, value):
        if key in self.DURATIONS:
            getattr(obj, key).FromJsonString(value)
        elif key in self.REPEATED:
            if value:
                getattr(obj, key).extend(value)
        else:
            setattr(obj, key, value)

# Создание парсера аргументов командной строки
def create_parser():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--host', default='smartspeech.sber.ru', help='хост и порт gRPC-конечной точки')
    parser.add_argument('--token', required=True, default=argparse.SUPPRESS, help='токен доступа')
    parser.add_argument('--file', required=True, default=argparse.SUPPRESS, help='аудиофайл для распознавания')

    parser.add_argument('--normalized-result', action='store_true', help='показывать нормализованный текст')
    parser.add_argument('--emotions-result', action='store_true', help='показывать результаты по эмоциям')

    parser.add_argument('--audio-encoding', default=ENCODINGS_MAP[ENCODING_PCM], type=lambda x: ENCODINGS_MAP[x], help=str(list(ENCODINGS_MAP)))
    parser.add_argument('--sample-rate', default=16000, type=int, help='только для PCM')
    parser.add_argument('--model', default='general', help=' ')
    parser.add_argument('--hypotheses-count', default=1, type=int, help=' ')
    parser.add_argument('--enable-profanity-filter', action='store_true', help=' ')
    parser.add_argument('--enable-multi-utterance', action='store_true', help=' ')
    parser.add_argument('--enable-partial-results', action='store_true', help=' ')
    parser.add_argument('--no-speech-timeout', default='7s', help=' ')
    parser.add_argument('--max-speech-timeout', default='20s', help=' ')
    parser.add_argument('--hints-words', nargs='*', default=[], help=' ')
    parser.add_argument('--hints-enable-letters', action='store_true', help=' ')
    parser.add_argument('--hints-eou-timeout', default='0s', help=' ')
    parser.add_argument('--channels-count', default=1, type=int, help=' ')

    return parser


def main():
    parser = create_parser()

    recognize(parser.parse_args(namespace=Arguments()))


if __name__ == '__main__':
    main()

